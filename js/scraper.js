const cheerio = require('cheerio');
const request = require('request');

// Export as object with request method 
module.exports = function (completed) {
    request('http://skjelin.com/portrait.html', (error,
        respone, html) => {
        if (!error && respone.statusCode == 200) {
            // console.log(html);
            const site = cheerio.load(html);

            //console.log(dom);
            const main = site('.model');
            const h1 = main.find('h1').text();
            const p = main.find('p').text();
            const imgURL = main.find('img').attr('src');

            let scrap = {
                header: h1,
                text: p,
                imageURL: imgURL
            }
            completed(scrap);
        }
    });
}

