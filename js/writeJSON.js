
const fs = require('fs');

function writeToJSON(scrap){
    let data = JSON.stringify(scrap, null, 2);
    fs.writeFileSync('scrap.json', data);
}
module.exports.writeToJSON = writeToJSON;